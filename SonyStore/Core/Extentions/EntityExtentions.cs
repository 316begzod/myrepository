﻿using SonyStore.Core.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SonyStore.Core.Extentions
{
    public static class EntityExtentions
    {
        public static string GetFullName(this IPerson person)
        {
            if (string.IsNullOrEmpty(person.LastName))
            {
                if (string.IsNullOrEmpty(person.MiddleName))
                {
                    return person.FirstName;
                }
                return string.Format("{0} {1}", person.FirstName, person.MiddleName);
            }
            else if (string.IsNullOrEmpty(person.MiddleName))
            {
                return string.Format("{0} {1}", person.FirstName, person.LastName);
            }
            return string.Format("{0} {1} {2}", person.FirstName, person.LastName, person.MiddleName);
        }
    }
}