﻿using SonyStore.Core.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SonyStore.Core.BLL
{
    public interface IRepository<TEntity>
        where TEntity : class
    {
        DbSet<TEntity> DbSet { get; set; }

        TEntity Create(TEntity entity);

        TEntity Update(TEntity entity);

        void Delete(TEntity entity);

    }

    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : class, IEntity
    {
        public Repository(DbContext dbContext)
        {
            DbContext = dbContext;
            DbSet = dbContext.Set<TEntity>();
        }

        public DbContext DbContext { get; set; }
        public DbSet<TEntity> DbSet { get; set; }

        public virtual TEntity Create(TEntity entity)
        {
            DbContext.Entry<TEntity>(entity).State = EntityState.Added;
            DbContext.SaveChanges();
            return entity;
        }

        public virtual void Delete(TEntity entity)
        {
            DbContext.Entry<TEntity>(entity).State = EntityState.Deleted;
            DbContext.SaveChanges();
        }

        public virtual TEntity Update(TEntity entity)
        {
            DbContext.Entry<TEntity>(entity).State = EntityState.Modified;
            DbContext.SaveChanges();
            return entity;
        }
    }
}