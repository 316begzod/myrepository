﻿using SonyStore.Core.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SonyStore.Core.BLL
{
    public interface IService<TEntity>
        where TEntity : class
    {
        IQueryable<TEntity> AllAsQuerable { get; }
        TEntity[] All { get; }
        int Count { get; }

        TEntity ById(int id);
        TEntity Create(TEntity entity);
        TEntity Update(TEntity entity);
        void Delete(TEntity entity);
        void Delete(IEnumerable<TEntity> entities);
        void Delete(int id);
    }

    public class Service<TEntity> : IService<TEntity>, IDisposable
        where TEntity : class, IEntity
    {
        #region Props

        public IRepository<TEntity> Repository { get; set; }

        public virtual IQueryable<TEntity> AllAsQuerable
        {
            get
            {
                return Repository.DbSet.AsQueryable();
            }
        }

        public virtual TEntity[] All
        {
            get
            {
                return Repository.DbSet.ToArray();
            }
        }

        public virtual int Count
        {
            get
            {
                return Repository.DbSet.Count();
            }
        }
        #endregion

        #region Ctor

        public Service(IRepository<TEntity> repository)
        {
            Repository = repository;
        }

        public Service(DbContext dbContext)
        {
            Repository = new Repository<TEntity>(dbContext);
        }
        #endregion

        #region Methods
        public virtual TEntity ById(int id)
        {
            return Repository.DbSet.Find(id);
        }

        public virtual TEntity Create(TEntity entity)
        {
            return Repository.Create(entity);
        }

        public virtual TEntity Update(TEntity entity)
        {
            return Repository.Update(entity);
        }

        public virtual void Delete(TEntity entity)
        {
            Repository.Delete(entity);
        }

        public virtual void Delete(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }

        public virtual void Delete(int id)
        {
            var entity = ById(id);
            if (entity != null)
            {
                Delete(entity);
            }
        }

        public virtual void Dispose()
        {
        }
        #endregion
    }
}