﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using SonyStore.Core.DAL;
using SonyStore.Core.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SonyStore.Core.BLL.Services
{
    public class PhoneService : Service<Phone>
    {
        #region Instance
        public static PhoneService Instance
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Get<PhoneService>();
            }
        }

        private PhoneService(ApplicationDbContext dbContext)
            :base(dbContext)
        {

        }

        public static PhoneService Create(IdentityFactoryOptions<PhoneService> options, IOwinContext context)
        {
            return new PhoneService(context.Get<ApplicationDbContext>());
        }
        #endregion

        public IList<SelectListItem> GetSelectList()
        {
            return AllAsQuerable.Select(w => new SelectListItem
            {
                Value = w.Id.ToString(),
                Text = w.Name
            }).ToList();
        }
    }
}