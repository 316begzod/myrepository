﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using SonyStore.Core.DAL;
using SonyStore.Core.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SonyStore.Core.BLL.Services
{
    public class CategoryService : Service<Category>
    {
        #region Instance
        public static CategoryService Instance
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Get<CategoryService>();
            }
        }

        private CategoryService(ApplicationDbContext dbContext)
            :base(dbContext)
        {

        }

        public static CategoryService Create(IdentityFactoryOptions<CategoryService> options, IOwinContext context)
        {
            return new CategoryService(context.Get<ApplicationDbContext>());
        }
        #endregion

        public IList<SelectListItem> GetSelectList()
        {
            return AllAsQuerable.Select(w => new SelectListItem
            {
                Value = w.Id.ToString(),
                Text = w.Name
            }).ToList();
        }
    }
}