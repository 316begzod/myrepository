﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using SonyStore.Core.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SonyStore.Core.BLL.Services
{
    public class RoleService : IDisposable
    {
        #region Instance
        public static RoleService Instance
        {
            get
            {
                return HttpContext.Current.GetOwinContext().Get<RoleService>();
            }
        }
        private ApplicationDbContext dbContext { get; set; }

        private RoleService(ApplicationDbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public static RoleService Create(IdentityFactoryOptions<RoleService> options, IOwinContext context)
        {
            return new RoleService(context.Get<ApplicationDbContext>());
        }
        #endregion

        public IQueryable<IdentityRole> AllAsQuerable { get { return dbContext.Roles.AsQueryable(); } }

        public List<SelectListItem> GetAsSelectList()
        {
            return AllAsQuerable.Select(x => new SelectListItem
            {
                Value = x.Id,
                Text = x.Name
            }).ToList();
        }

        public IdentityRole ById(string id)
        {
            return dbContext.Roles.Find(id);
        }

        public void Dispose()
        {
            try
            {
                dbContext.Dispose();
            }
            catch { }
        }

        public IdentityRole Create(IdentityRole ent)
        {
            dbContext.Entry(ent).State = System.Data.Entity.EntityState.Added;
            dbContext.SaveChanges();
            return ent;
        }

        public IdentityRole Update(IdentityRole ent)
        {
            dbContext.Entry(ent).State = System.Data.Entity.EntityState.Modified;
            dbContext.SaveChanges();
            return ent;
        }

        public void Delete(IdentityRole ent)
        {
            dbContext.Entry(ent).State = System.Data.Entity.EntityState.Deleted;
            dbContext.SaveChanges();
        }
    }
}