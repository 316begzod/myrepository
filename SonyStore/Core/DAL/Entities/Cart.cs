﻿using SonyStore.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SonyStore.Core.DAL.Entities
{
    public class Cart : Entity
    {
        public int? PurchasesCount { get; set; }

        public int? Total { get; set; }

        public string ApplicationUserId { get; set; }

        public ApplicationUser ApplicationUser { get; set; }
    }
}