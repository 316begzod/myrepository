﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyStore.Core.DAL.Entities
{
    public class Category : Entity
    {
        public Category()
        {
            Phones = new HashSet<Phone>();
        }

        [Required]
        public string Name { get; set; }

        public string Description { get; set; }

        public ICollection<Phone> Phones { get; set; }
    }
}