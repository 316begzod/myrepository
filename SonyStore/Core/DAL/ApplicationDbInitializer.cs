﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace SonyStore.Core.DAL
{
    public class ApplicationDbInitializer :
        MigrateDatabaseToLatestVersion<ApplicationDbContext,
                                        ApplicationDbMigrationsConfiguration>
    {
        public ApplicationDbInitializer()
            : base()
        {

        }
    }
}