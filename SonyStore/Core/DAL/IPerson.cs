﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SonyStore.Core.DAL
{
    public interface IPerson
    {
        string FirstName { get; set; }
        string LastName { get; set; }
        string MiddleName { get; set; }
    }
}