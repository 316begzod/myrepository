﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SonyStore.Startup))]
namespace SonyStore
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
