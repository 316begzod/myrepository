﻿using SonyStore.Areas.Dashboard.Models;
using SonyStore.Core.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SonyStore.Controllers
{
    public class PhoneController : Controller
    {
        public ApplicationUserManager UserService { get { return ApplicationUserManager.Instance; } }
        public PhoneService PService { get { return PhoneService.Instance; } }

        // GET: Dashboard/Phone
        public ActionResult Index()
        {
            var query = PService.AllAsQuerable.AsEnumerable();
            var models = new List<PhoneViewModel>();

            foreach (var ent in query)
            {
                var model = new PhoneViewModel();
                model.LoadEntity(ent);
                models.Add(model);
            }


            return View(models);
        }

        // GET: Dashboard/Phone/Details/5
        public ActionResult Phone(int id)
        {
            var type = PService.ById(id);
            var model = new PhoneViewModel();
            model.LoadEntity(type);
            return View(model);
        }
    }
}