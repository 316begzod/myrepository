﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;

namespace SonyStore.App_Start
{
    public class AppSettings
    {
        private static string uploadDir;

        public static string UploadDir
        {
            get
            {
                if (string.IsNullOrEmpty(uploadDir))
                {
                    uploadDir = ConfigurationManager.AppSettings["UploadDir"];
                    CreateDirIfNotExist(uploadDir);
                }
                return uploadDir;
            }
        }

        private static void CreateDirIfNotExist(string path)
        {
            path = HttpContext.Current.Server.MapPath(path);
            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }
        }
    }
}