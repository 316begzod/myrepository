﻿using SonyStore.App_Start;
using SonyStore.Areas.Dashboard.Models;
using SonyStore.Core.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SonyStore.Areas.Dashboard.Controllers
{
    public class PhoneController : Controller
    {
        public ApplicationUserManager UserService { get { return ApplicationUserManager.Instance; } }
        public PhoneService PService { get { return PhoneService.Instance; } }

        // GET: Dashboard/Phone
        public ActionResult Index()
        {
            var query = PService.AllAsQuerable.AsEnumerable();
            var models = new List<PhoneViewModel>();

            foreach (var ent in query)
            {
                var model = new PhoneViewModel();
                model.LoadEntity(ent);
                models.Add(model);
            }


            return View(models);
        }

        // GET: Dashboard/Phone/Details/5
        public ActionResult Details(int id)
        {
            var type = PService.ById(id);
            var model = new PhoneViewModel();
            model.LoadEntity(type);
            return View(model);
        }

        // GET: Dashboard/Phone/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dashboard/Phone/Create
        [HttpPost]
        public ActionResult Create(PhoneViewModel model)
        {
            if (ModelState.IsValid)
            {
                var product = model.CreateEntity();

                var newName = "";
                if (model.Photo != null)
                {
                    var ext = GetFileExtention(model.Photo.FileName);
                    newName = Guid.NewGuid().ToString() + ext;
                    var fileName = Server.MapPath(AppSettings.UploadDir + newName);
                    model.Photo.SaveAs(fileName);
                }
                else
                {
                    newName = "DefaultPhoto.jpg";
                    var fileName = Server.MapPath(AppSettings.UploadDir + newName);
                    model.Path = fileName;
                }

                PService.Create(product);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Dashboard/Phone/Edit/5
        public ActionResult Edit(int id)
        {
            var ent = PService.ById(id);
            var model = new PhoneViewModel();
            model.LoadEntity(ent);
            return View(model);
        }

        // POST: Dashboard/Phone/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, PhoneViewModel model)
        {
            if (ModelState.IsValid)
            {
                var product = PService.ById(model.Id);
                product = model.UpdateEntity(product);
                if (model.Photo != null)
                {
                    var fileName = Server.MapPath(AppSettings.UploadDir + product.Path);
                    if (System.IO.File.Exists(fileName))
                    {
                        System.IO.File.Delete(fileName);
                    }
                    var ext = GetFileExtention(model.Photo.FileName);
                    var newName = Guid.NewGuid().ToString() + ext;
                    fileName = Server.MapPath(AppSettings.UploadDir + newName);
                    model.Photo.SaveAs(fileName);
                    product.Path = newName;
                }

                PService.Update(product);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        [NonAction]
        private string GetFileExtention(string FileName)
        {
            var i = FileName.LastIndexOf(".");
            if (i != -1)
            {
                return FileName.Substring(i);
            }
            return "";
        }

        // GET: Dashboard/Phone/Delete/5
        public ActionResult Delete(int id)
        {
            var product = PService.ById(id);
            if (product == null)
                return RedirectToAction("Index");
            var model = new PhoneViewModel();
            model.LoadEntity(product);
            return View(model);
        }

        // POST: Dashboard/Phone/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                PService.Delete(id);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
