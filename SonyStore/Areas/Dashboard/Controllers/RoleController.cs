﻿using Microsoft.AspNet.Identity.EntityFramework;
using SonyStore.Areas.Dashboard.Models;
using SonyStore.Core.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SonyStore.Areas.Dashboard.Controllers
{
    public class RoleController : Controller
    {
        // GET: Dashboard/Role
        public ActionResult Index()
        {
            var models = RoleService.Instance.AllAsQuerable
                                    .Select(w => new RoleViewModel
                                    {
                                        Id = w.Id,
                                        Name = w.Name
                                    });
            return View(models);
        }

        // GET: Dashboard/Role/Details/5
        public ActionResult Details(string id)
        {
            var role = RoleService.Instance.ById(id);
            var model = new RoleViewModel
            {
                Id = role.Id,
                Name = role.Name
            };
            return View(model);
        }

        // GET: Dashboard/Role/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dashboard/Role/Create
        [HttpPost]
        public ActionResult Create(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var ent = new IdentityRole
                {
                    Name = model.Name
                };
                ent = RoleService.Instance.Create(ent);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Dashboard/Role/Edit/5
        public ActionResult Edit(string id)
        {
            var role = RoleService.Instance.ById(id);
            var model = new RoleViewModel
            {
                Id = role.Id,
                Name = role.Name
            };
            return View(model);
        }

        // POST: Dashboard/Role/Edit/5
        [HttpPost]
        public ActionResult Edit(RoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                var role = RoleService.Instance.ById(model.Id);
                role.Name = model.Name;
                role = RoleService.Instance.Update(role);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        // GET: Dashboard/Role/Delete/5
        public ActionResult Delete(string id)
        {
            var role = RoleService.Instance.ById(id);
            var model = new RoleViewModel
            {
                Id = role.Id,
                Name = role.Name
            };
            return View(model);
        }

        // POST: Dashboard/Role/Delete/5
        [HttpPost]
        public ActionResult Delete(RoleViewModel model)
        {
            try
            {
                var ent = RoleService.Instance.ById(model.Id);
                RoleService.Instance.Delete(ent);
                return RedirectToAction("Index");
            }
            catch
            {
                return View(model);
            }
        }

    }
}
