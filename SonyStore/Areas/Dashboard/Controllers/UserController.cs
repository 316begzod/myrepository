﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using SonyStore.Areas.Dashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace SonyStore.Areas.Dashboard.Controllers
{
    public class UserController : Controller
    {
        private ApplicationUserManager _userManager;

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
        // GET: Dashboard/Foydalanuvchi
        public ActionResult Index()
        {
            List<UserViewModel> models = new List<UserViewModel>();
            foreach (var user in UserManager.Users)
            {
                var model = new UserViewModel();
                model.LoadEntity(user);
                models.Add(model);
            }
            return View(models);
        }

        public ActionResult Edit(string id)
        {
            var user = UserManager.FindById(id);
            var model = new UserViewModel();
            model.LoadEntity(user);
            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(UserViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = UserManager.FindById(model.Id);
                user = model.UpdateEntity(user);
                UserManager.Update(user);
                return RedirectToAction("Index");
            }
            return View(model);
        }

        public ActionResult Details(string id)
        {
            var user = UserManager.FindById(id);
            var model = new UserViewModel();
            model.LoadEntity(user);
            return PartialView(model);
        }

        public ActionResult Delete(string id)
        {
            var user = UserManager.FindById(id);
            UserManager.Delete(user);
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }

        public ActionResult ResetPassword(string id)
        {
            var model = new ResetViewModel
            {
                Id = id
            };

            return PartialView(model);
        }

        [HttpPost]
        public ActionResult ResetPassword(string id, ResetViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var result = UserManager.RemovePassword(model.Id);
            if (result.Succeeded)
            {
                result = UserManager.AddPassword(model.Id, model.NewPassword);
                if (result.Succeeded)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.OK);
                }
            }

            return View();
        }
    }
}
