﻿using SonyStore.Areas.Dashboard.Models;
using SonyStore.Core.BLL.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SonyStore.Areas.Dashboard.Controllers
{
    public class CartController : Controller
    {
        public ApplicationUserManager UserService { get { return ApplicationUserManager.Instance; } }
        public CartService PService { get { return CartService.Instance; } }

        // GET: Dashboard/Phone
        public ActionResult Index()
        {
            var query = PService.AllAsQuerable.AsEnumerable();
            var models = new List<CartViewModel>();

            foreach (var ent in query)
            {
                var model = new CartViewModel();
                model.LoadEntity(ent);
                models.Add(model);
            }


            return View(models);
        }

        // GET: Dashboard/Phone/Details/5
        public ActionResult Details(int id)
        {
            var type = PService.ById(id);
            var model = new CartViewModel();
            model.LoadEntity(type);
            return View(model);
        }

        // GET: Dashboard/Phone/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Dashboard/Phone/Create
        [HttpPost]
        public ActionResult Create(CartViewModel model)
        {
            if (ModelState.IsValid)
            {
                var ent = model.CreateEntity();
                PService.Create(ent);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        // GET: Dashboard/Phone/Edit/5
        public ActionResult Edit(int id)
        {
            var ent = PService.ById(id);
            var model = new CartViewModel();
            model.LoadEntity(ent);
            return View(model);
        }

        // POST: Dashboard/Phone/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, CartViewModel model)
        {
            if (ModelState.IsValid)
            {
                var product = PService.ById(id);
                product = model.UpdateEntity(product);
                PService.Update(product);
                return RedirectToAction("Index");
            }
            else
            {
                return View();
            }
        }

        // GET: Dashboard/Phone/Delete/5
        public ActionResult Delete(int id)
        {
            var product = PService.ById(id);
            if (product == null)
                return RedirectToAction("Index");
            var model = new CartViewModel();
            model.LoadEntity(product);
            return View(model);
        }

        // POST: Dashboard/Phone/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
