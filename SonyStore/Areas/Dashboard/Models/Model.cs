﻿using SonyStore.Core.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SonyStore.Areas.Dashboard.Models
{
    public interface IModel<TEntity>
        where TEntity : class, IEntity
    {
        int Id { get; set; }
        void LoadEntity(TEntity ent);
        TEntity CreateEntity();
        TEntity UpdateEntity(TEntity ent);
    }

    public class Model<TEntity> : IModel<TEntity>
        where TEntity : class, IEntity
    {
        public int Id { get; set; }

        public virtual TEntity CreateEntity()
        {
            var type = GetType();
            var entType = typeof(TEntity);

            var ent = Activator.CreateInstance<TEntity>();//TEntity tipida bitta obyekt oladi

            var propInfos = type.GetProperties();//Base model propertylari
            foreach (var propInfo in propInfos)
            {
                var entPropInfo = entType.GetProperty(propInfo.Name);
                if (entPropInfo != null)
                {
                    if (propInfo.PropertyType.IsInstanceOfType(typeof(Model<>))) continue;
                    if (propInfo.PropertyType.IsInstanceOfType(typeof(IList<>))) continue;

                    try
                    {
                        entPropInfo.SetValue(ent, propInfo.GetValue(this));
                    }
                    catch { }
                }
            }
            return ent;
        }

        public virtual void LoadEntity(TEntity ent)
        {
            var type = GetType();//base model ya'ni SingerViewModel
            var entType = typeof(TEntity);//model tipi M: Singer

            var propInfos = type.GetProperties();//Base model propertylari(M: SingerViewModel)
            foreach (var propInfo in propInfos)
            {
                var entPropInfo = entType.GetProperty(propInfo.Name);
                if (entPropInfo != null)
                {
                    if (propInfo.PropertyType.IsInstanceOfType(typeof(Model<>))) continue;
                    if (propInfo.PropertyType.IsInstanceOfType(typeof(IList<>))) continue;
                    try
                    {
                        propInfo.SetValue(this, entPropInfo.GetValue(ent));
                    }
                    catch { }
                }
            }
        }

        public virtual TEntity UpdateEntity(TEntity ent)
        {
            var type = GetType();//base model ya'ni SingerViewModel
            var entType = typeof(TEntity);//model tipi M: Singer

            var propInfos = type.GetProperties();//Base model propertylari(M: SingerViewModel)
            foreach (var propInfo in propInfos)
            {
                var entPropInfo = entType.GetProperty(propInfo.Name);
                if (entPropInfo != null)
                {
                    if (propInfo.PropertyType.IsInstanceOfType(typeof(Model<>))) continue;
                    if (propInfo.PropertyType.IsInstanceOfType(typeof(IList<>))) continue;
                    try
                    {
                        entPropInfo.SetValue(ent, propInfo.GetValue(this));
                    }
                    catch { }
                }
            }

            return ent;
        }
    }
}