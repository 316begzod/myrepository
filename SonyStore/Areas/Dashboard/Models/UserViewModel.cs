﻿using Microsoft.AspNet.Identity.EntityFramework;
using SonyStore.Core.BLL.Services;
using SonyStore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyStore.Areas.Dashboard.Models
{
    public class UserViewModel 
    {
        public UserViewModel()
        {
            Roles = new List<string>();
        }

        public string Id { get; set; }

        public string UserName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }

        public string RoleName { get; set; }

        public List<string> Roles { get; set; }

        public void LoadEntity(ApplicationUser user)
        {
            Roles = user.Roles.Select(w => w.RoleId).ToList();
            RoleName = "";

            foreach (var roleId in Roles)
            {
                if (!string.IsNullOrEmpty(RoleName)) RoleName += ", ";
                RoleName += RoleService.Instance.ById(roleId).Name;
            }
        }

        public ApplicationUser UpdateEntity(ApplicationUser user)
        {
            var deleteListRoles = new List<IdentityUserRole>();
            foreach (var item in user.Roles)
            {
                if (!Roles.Contains(item.RoleId))
                {
                    deleteListRoles.Add(item);
                }
            }

            foreach (var item in deleteListRoles)
            {
                user.Roles.Remove(item);
            }

            foreach (var roleId in Roles)
            {
                if (!user.Roles.Any(w => w.RoleId == roleId))
                {
                    user.Roles.Add(new IdentityUserRole
                    {
                        RoleId = roleId
                    });
                }
            }
            return user;
        }
    }
}