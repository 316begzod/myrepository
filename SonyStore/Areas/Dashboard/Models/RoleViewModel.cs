﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyStore.Areas.Dashboard.Models
{
    public class RoleViewModel
    {
        public string Id { get; set; }

        [Required(ErrorMessage = "Поля не выполнено")]
        [Display(Name = "Название")]
        [MaxLength(256)]
        public string Name { get; set; }
    }
}