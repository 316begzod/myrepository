﻿using SonyStore.Core.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SonyStore.Areas.Dashboard.Models
{
    public class CartViewModel : Model<Cart>
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public void AddItem(PhoneViewModel phone, int quantity)
        {
            CartLine line = lineCollection
                .Where(g => g.Phone.Id == phone.Id)
                .FirstOrDefault();

            if (line == null)
            {
                lineCollection.Add(new CartLine
                {
                    Phone = phone,
                    Quantity = quantity
                });
            }
            else
            {
                line.Quantity += quantity;
            }
        }

        public void RemoveLine(PhoneViewModel phone)
        {
            lineCollection.RemoveAll(l => l.Phone.Id == phone.Id);
        }

        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(e => e.Phone.Price * e.Quantity);

        }
        public void Clear()
        {
            lineCollection.Clear();
        }

        public IEnumerable<CartLine> Lines
        {
            get { return lineCollection; }
        }
    }

    public class CartLine
    {
        public PhoneViewModel Phone { get; set; }
        public int Quantity { get; set; }
    }
}