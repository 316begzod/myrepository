﻿using SonyStore.Core.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyStore.Areas.Dashboard.Models
{
    public class CategoryViewModel : Model<Category>
    {
        [Required(ErrorMessage = "Nomini kiriting")]
        public string Name { get; set; }

        public string Description { get; set; }
    }
}