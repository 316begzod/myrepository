﻿using SonyStore.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyStore.Areas.Dashboard.Models
{
    public class ResetViewModel
    {
        public string Id { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "Пароль должен быть минимум ", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Пароль")]
        public string NewPassword { get; set; }

        //[DataType(DataType.Password)]
        //[Display(Name = "Потверждение пароля")]
        //[Compare("Password", ErrorMessage = "Пароли не совподает")]
        //public string ConfirmPassword { get; set; }

        public void LoadEntity(ApplicationUser user)
        {
            Id = Id;
        }
    }
}