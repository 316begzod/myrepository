﻿using SonyStore.Core.DAL.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SonyStore.Areas.Dashboard.Models
{
    public class PhoneViewModel : Model<Phone>
    {
        [Required(ErrorMessage = "Nomini kiriting")]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Display(Name = "Rasm")]
        public HttpPostedFileBase Photo { get; set; }

        public string Path { get; set; }

        public string CategoryName { get; set; }

        [Required(ErrorMessage = "Categoriyani tanlang")]
        public int CategoryId { get; set; }

        [Required(ErrorMessage = "Narxini Kiriting")]
        public decimal Price { get; set; }

    }
}